package es.laurafguis.testbycode.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.laurafguis.testbycode.domain.entity.Question;
import es.laurafguis.testbycode.domain.service.IQuestionService;
import es.laurafguis.testbycode.domain.service.IQuizService;
import es.laurafguis.testbycode.presentation.controller.QuestionController;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@ExtendWith(MockitoExtension.class)
public class QuestionControllerTest {

    @Mock
    private IQuestionService questionService;

    @Mock
    private IQuizService quizService;

    @InjectMocks
    private QuestionController questionController;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(questionController).build();
    }

    @Test
    @DisplayName("Should Save Valid Question Successfully")
    public void shouldSaveValidQuestionSuccessfully() throws Exception {
        Question question = new Question();
        question.setQuestionId(1L);
        question.setContent("Test Question");

        when(questionService.create(any(Question.class))).thenReturn(question);

        mockMvc.perform(post("/question/")
                        .content(new ObjectMapper().writeValueAsString(question))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Update Valid Question Successfully")
    public void shouldUpdateValidQuestionSuccessfully() throws Exception {
        Question question = new Question();
        question.setQuestionId(1L);
        question.setContent("Updated Question");

        when(questionService.update(any(Question.class))).thenReturn(question);

        mockMvc.perform(put("/question/")
                        .content(new ObjectMapper().writeValueAsString(question))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}