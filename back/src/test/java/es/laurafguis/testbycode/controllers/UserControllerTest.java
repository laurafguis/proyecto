package es.laurafguis.testbycode.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.entity.UserRol;
import es.laurafguis.testbycode.domain.service.IUserService;
import es.laurafguis.testbycode.presentation.controller.UserController;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

    @Mock
    private IUserService userService;

    @InjectMocks
    private UserController userController;

    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    @DisplayName("Should Return All Users Successfully")
    public void shouldReturnAllUsersSuccessfully() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUsername("Test User");

        List<User> users = Arrays.asList(user);

        when(userService.getAll()).thenReturn((Stream<User>) users);

        mockMvc.perform(get("/users/")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Return User By Id Successfully")
    public void shouldReturnUserByIdSuccessfully() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUsername("Test User");

        when(userService.find(anyLong())).thenReturn(user);

        mockMvc.perform(get("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Save User Successfully")
    public void shouldSaveUserSuccessfully() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUsername("Test User");

        UserRol userRol = new UserRol();
        Set<UserRol> userRoles = new HashSet<>();
        userRoles.add(userRol);

        when(userService.createUser(any(User.class), any(Set.class))).thenReturn(user);

        // Convert User object to JSON
        ObjectMapper objectMapper = new ObjectMapper();
        String userJson = objectMapper.writeValueAsString(user);

        mockMvc.perform(post("/users/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(userJson))
                .andExpect(status().isOk());
    }
    @Test
    @DisplayName("Should Return User By Username Successfully")
    public void shouldReturnUserByUsernameSuccessfully() throws Exception {
        User user = new User();
        user.setId(1L);
        user.setUsername("Test User");

        when(userService.findByName(any(String.class))).thenReturn(user);

        mockMvc.perform(get("/users/username/Test User")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Should Delete User Successfully")
    public void shouldDeleteUserSuccessfully() throws Exception {
        mockMvc.perform(delete("/users/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
