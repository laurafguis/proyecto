package es.laurafguis.testbycode.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.service.impl.UserDetailsServiceImpl;
import es.laurafguis.testbycode.presentation.request.JwtRequest;
import es.laurafguis.testbycode.security.JwtUtils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuthenticationManager authenticationManager;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    @MockBean
    private JwtUtils jwtUtils;

    @Test
    @DisplayName("Test Generate Token Endpoint")
    public void testGeneraToken() throws Exception {
        String username = "testUser";
        String password = "testPassword";
        String token = "testToken";
        JwtRequest jwtRequest = new JwtRequest();
        jwtRequest.setUsername(username);
        jwtRequest.setPassword(password);

        User userDetails = new User();
        userDetails.setUsername(username);
        userDetails.setPassword(password);

        when(userDetailsService.loadUserByUsername(username)).thenReturn(userDetails);
        when(jwtUtils.generateToken(userDetails)).thenReturn(token);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/generate-token")
                        .content(new ObjectMapper().writeValueAsString(jwtRequest))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}