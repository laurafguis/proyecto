package es.laurafguis.testbycode.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.service.ICategoryService;
import es.laurafguis.testbycode.presentation.controller.CategoryController;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

public class CategoryControllerTest {

    private MockMvc mockMvc;

    @Mock
    private ICategoryService categoryService;

    @InjectMocks
    private CategoryController categoryController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(categoryController).build();
    }

    @Test
    @DisplayName("Test Save Category")
    public void testSaveCategory() throws Exception {
        Category category = new Category();
        category.setCategoryId(1L);
        category.setTitle("Test Category");

        when(categoryService.create(category)).thenReturn(category);

        mockMvc.perform(post("/category/")
                        .content(new ObjectMapper().writeValueAsString(category))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test Get Category By Id")
    public void testGetCategoryById() throws Exception {
        Category category = new Category();
        category.setCategoryId(1L);
        category.setTitle("Test Category");

        when(categoryService.find(1L)).thenReturn(category);

        mockMvc.perform(get("/category/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(category)));
    }

    @Test
    @DisplayName("Test Get All Categories")
    public void testGetCategories() throws Exception {
        Category category1 = new Category();
        category1.setCategoryId(1L);
        category1.setTitle("Test Category 1");

        Category category2 = new Category();
        category2.setCategoryId(2L);
        category2.setTitle("Test Category 2");

        Set<Category> categories = new HashSet<>(Arrays.asList(category1, category2));

        when(categoryService.getAll()).thenReturn(categories);

        mockMvc.perform(get("/category/")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(categories)));
    }

    @Test
    @DisplayName("Test Update Category")
    public void testUpdateCategories() throws Exception {
        Category category = new Category();
        category.setCategoryId(1L);
        category.setTitle("Updated Category");

        when(categoryService.update(category)).thenReturn(category);

        mockMvc.perform(put("/category/")
                        .content(new ObjectMapper().writeValueAsString(category))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Test Delete Category")
    public void testDeleteCategory() throws Exception {
        mockMvc.perform(delete("/category/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}