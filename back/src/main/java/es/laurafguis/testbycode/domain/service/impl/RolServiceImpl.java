package es.laurafguis.testbycode.domain.service.impl;

import es.laurafguis.testbycode.domain.entity.Rol;
import es.laurafguis.testbycode.domain.service.IRolService;
import es.laurafguis.testbycode.persistence.RolRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolServiceImpl implements IRolService {

    @Autowired
    private RolRepository rolRepository;

    @Override
    public Rol findByRolName(String rolName) {
        return rolRepository.findByRolName(rolName);
    }
}
