package es.laurafguis.testbycode.domain.service;

import java.util.Set;

import es.laurafguis.testbycode.domain.entity.Category;

public interface ICategoryService {

    Category create(Category category);

    Category update(Category category);

    Set<Category> getAll();

    Category find(Long categoryId);

    void delete(Long categoryId);


}
