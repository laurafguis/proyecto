package es.laurafguis.testbycode.domain.service;

import es.laurafguis.testbycode.domain.entity.Rol;

public interface IRolService {

    Rol findByRolName(String rolName);
}
