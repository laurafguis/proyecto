package es.laurafguis.testbycode.domain.service.impl;

import es.laurafguis.testbycode.domain.entity.Question;
import es.laurafguis.testbycode.domain.entity.Quiz;
import es.laurafguis.testbycode.domain.service.IQuestionService;
import es.laurafguis.testbycode.persistence.QuestionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;

@Service
public class QuestionServiceImpl implements IQuestionService {

    @Autowired
    private QuestionRepository questionRepository;

    @Override
    public Question create(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public Question update(Question question) {
        return questionRepository.save(question);
    }

    @Override
    public Set<Question> getAll() {
        return new HashSet<>(questionRepository.findAll());
    }

    @Override
    public Question find(Long questionId) {
        return questionRepository.findById(questionId).orElseThrow(() -> new EntityNotFoundException("Question not found"));
    }

    @Override
    public Set<Question> getQuestionsFromQuiz(Quiz quiz) {
        return new HashSet<>(questionRepository.findByQuiz(quiz));
    }

    @Override
    public void delete(Long questionId) {
        Question question = new Question();
        question.setQuestionId(questionId);
        questionRepository.delete(question);
    }

    @Override
    public Question listQuestion(Long questionId) {
        return questionRepository.findById(questionId).orElseThrow(() -> new EntityNotFoundException("Question not found"));
    }
}
