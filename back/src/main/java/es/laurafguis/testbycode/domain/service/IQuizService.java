package es.laurafguis.testbycode.domain.service;

import java.util.List;
import java.util.Set;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.entity.Quiz;

public interface IQuizService {

        Quiz create(Quiz quiz);

        Quiz update(Quiz quiz);

        Set<Quiz> getAll();

        Quiz find(Long quizId);

        void delete(Long quizId);

        List<Quiz> listQuizsByCategory(Category category);

        List<Quiz> getActivoQuizs();

        List<Quiz> getActivoQuizsByCategory(Category category);
}


