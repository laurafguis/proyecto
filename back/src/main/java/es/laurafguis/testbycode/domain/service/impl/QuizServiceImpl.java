package es.laurafguis.testbycode.domain.service.impl;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.entity.Quiz;
import es.laurafguis.testbycode.domain.service.IQuizService;
import es.laurafguis.testbycode.persistence.QuizRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;


@Service
public class QuizServiceImpl implements IQuizService {

    @Autowired
    private QuizRepository quizRepository;

    @Override
    public Quiz create(Quiz quiz) {
        return quizRepository.save(quiz);
    }

    @Override
    public Quiz update(Quiz quiz) {
        return quizRepository.save(quiz);
    }

    @Override
    public Set<Quiz> getAll() {
        return new LinkedHashSet<>(quizRepository.findAll());
    }

    @Override
    public Quiz find(Long quizId) {
        return quizRepository.findById(quizId).orElseThrow(() -> new EntityNotFoundException("Quiz not found"));
    }

    @Override
    public void delete(Long quizId) {
        Quiz quiz = new Quiz();
        quiz.setQuizId(quizId);
        quizRepository.delete(quiz);
    }

    @Override
    public List<Quiz> listQuizsByCategory(Category category) {
        return this.quizRepository.findByCategory(category);
    }

    @Override
    public List<Quiz> getActivoQuizs() {
        return quizRepository.findByActivo(true);
    }

    @Override
    public List<Quiz> getActivoQuizsByCategory(Category category) {
        return quizRepository.findByCategoryAndActivo(category, true);
    }

}
