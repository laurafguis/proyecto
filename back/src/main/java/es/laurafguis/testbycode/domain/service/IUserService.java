package es.laurafguis.testbycode.domain.service;

import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.entity.UserRol;
import es.laurafguis.testbycode.presentation.request.UserRequest;

public interface IUserService {

    Stream<User> getAll(Integer page, Integer pageSize);
    
    Stream<User> getAll();

    User find(Long userId);

    User findByName(String username);

    User saveUser(UserRequest userRequest) throws Exception;

    User createUser(User user, Set<UserRol> userRoles) throws Exception;

    void delete(Long userId);

    long getTotalNumberOfRecords();



}
