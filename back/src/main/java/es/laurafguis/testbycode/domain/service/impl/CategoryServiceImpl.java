package es.laurafguis.testbycode.domain.service.impl;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.service.ICategoryService;
import es.laurafguis.testbycode.persistence.CategoryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.LinkedHashSet;
import java.util.Set;

@Service
public class CategoryServiceImpl implements ICategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public Category create(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Category update(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    public Set<Category> getAll() {
        return new LinkedHashSet<>(categoryRepository.findAll());
    }

    @Override
    public Category find(Long categoryId) {
        return categoryRepository.findById(categoryId).orElseThrow(() -> new EntityNotFoundException("Category not found"));
    }

    @Override
    public void delete(Long categoryId) {
        Category category = new Category();
        category.setCategoryId(categoryId);
        categoryRepository.delete(category);
    }
}