package es.laurafguis.testbycode.domain.service;

import java.util.Set;

import es.laurafguis.testbycode.domain.entity.Question;
import es.laurafguis.testbycode.domain.entity.Quiz;

public interface IQuestionService {

    Question create(Question question);

    Question update(Question question);

    Set<Question> getAll();

    Question find(Long questionId);

    Set<Question> getQuestionsFromQuiz(Quiz quiz);

    void delete(Long questionId);

    Question listQuestion(Long questionId);
}
