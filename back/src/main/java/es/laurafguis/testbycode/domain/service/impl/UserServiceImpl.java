package es.laurafguis.testbycode.domain.service.impl;

import es.laurafguis.testbycode.domain.entity.Rol;
import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.entity.UserRol;
import es.laurafguis.testbycode.domain.service.IUserService;
import es.laurafguis.testbycode.persistence.RolRepository;
import es.laurafguis.testbycode.persistence.UserRepository;

import es.laurafguis.testbycode.presentation.request.RolRequest;
import es.laurafguis.testbycode.presentation.request.UserRequest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

@Service
public class UserServiceImpl implements IUserService {

    private final UserRepository userRepository;

    private final RolRepository rolRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;


    public UserServiceImpl(UserRepository userRepository, RolRepository rolRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.rolRepository = rolRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }


    public Stream<User> getAll(Integer page, Integer pageSize) {
        Pageable pageable = PageRequest.of(page - 1, pageSize);
        return userRepository.findAll(pageable).getContent().stream();
    }
    @Override
    public Stream<User> getAll() {
        return userRepository.findAll().stream();
    }

    @Override
    public User find(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }
    @Transactional
    public User saveUser(UserRequest userRequest) {
        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setEmail(userRequest.getEmail());
        user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setPhone(userRequest.getPhone());
        user.setProfile(userRequest.getProfile());
        user.setEnabled(true);

        Set<UserRol> userRoles = new HashSet<>();
        for (RolRequest roleRequest : userRequest.getRoles()) {
            Rol rol = rolRepository.findByRolName(roleRequest.getRolName());
            if (rol == null) {
                throw new RuntimeException("Role not found: " + roleRequest.getRolName());
            }
            UserRol userRol = new UserRol();
            userRol.setUser(user);
            userRol.setRol(rol);
            userRoles.add(userRol);
        }

        user.setUserRoles(userRoles);
        return userRepository.save(user);
    }

    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


//
//    @Transactional
//    public User create(User user) {
//        user.setUsername(user.getUsername());
//        user.setEmail(user.getEmail());
//        user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
//        user.setFirstName(user.getFirstName());
//        user.setLastName(user.getLastName());
//        user.setPhone(user.getPhone());
//        user.setProfile(user.getProfile());
//        user.setEnabled(true);
//
//        Set<UserRol> userRoles = new HashSet<>();
//        for (UserRol rol : user.getUserRoles()) {
//            rolRepository.findByRolName(rol.getRol().getRolName());
//            UserRol userRol = new UserRol();
//            userRol.setUser(user);
//            userRol.setRol(rol.getRol());
//            userRoles.add(userRol);
//        }
//
//        user.setUserRoles(userRoles);
//        return userRepository.save(user);
//    }

    @Override
    public User createUser(User user, Set<UserRol> userRoles) throws Exception {
        return null;
    }

    @Override
    public User findByName(String username) {
        return userRepository.findByUsername(username);
    }





    @Override
    public void delete(Long userId) {
        User user = this.find(userId);
        userRepository.delete(user);
    }

    @Override
    public long getTotalNumberOfRecords() {
        return userRepository.count();
    }



}

