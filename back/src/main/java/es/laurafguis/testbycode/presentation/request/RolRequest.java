package es.laurafguis.testbycode.presentation.request;

import lombok.Data;


@Data
public class RolRequest {

    private String rolName;

    // Constructor
    public RolRequest(String rolName) {
        this.rolName = rolName;
    }

    // Getter
    public String getRolName() {
        return rolName;
    }

    // Setter
    public void setRolName(String rolName) {
        this.rolName = rolName;
    }

    public RolRequest() {
    }
}
