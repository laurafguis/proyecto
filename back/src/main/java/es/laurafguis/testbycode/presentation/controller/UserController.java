package es.laurafguis.testbycode.presentation.controller;

import es.laurafguis.testbycode.domain.entity.Rol;
import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.entity.UserRol;
import es.laurafguis.testbycode.domain.service.IUserService;
import es.laurafguis.testbycode.persistence.RolRepository;
import es.laurafguis.testbycode.presentation.http_response.Response;

import es.laurafguis.testbycode.presentation.request.UserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Stream;


@RestController
@RequestMapping(UserController.USERS)
@CrossOrigin("*")
public class UserController {

    @Value("${application.url}")
    private String urlBase;

    @Value("${page.size}")
    private int PAGE_SIZE;

    public static final String USERS = "/users";

    @Autowired
    private IUserService userService;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @ResponseStatus(HttpStatus.OK)
    @GetMapping("")
    public Response getAll(@RequestParam(required = false) Integer page,
                           @RequestParam(required = false) Integer pageSize) {
        pageSize = (pageSize != null) ? pageSize : PAGE_SIZE;
        Stream<User> userStream = (page != null) ? userService.getAll(page, pageSize) : userService.getAll();

        long totalRecords = userService.getTotalNumberOfRecords();
        Response response = Response.builder()
                .data(userStream)
                .totalRecords(totalRecords)
                .build();

        if (page != null) {
            response.paginate(page, pageSize, urlBase);
        }

        return response;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{userId}")
    public User findUserById(@PathVariable Long userId) {

       return userService.find(userId);
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping("/{username}")
    public User getUser(@PathVariable String username) {
       return userService.findByName(username);
    }

//    @PostMapping("/")
//    public User saveUser(@RequestBody User user) throws Exception{
//        user.setProfile("default.png");
//        user.setPassword(this.bCryptPasswordEncoder.encode(user.getPassword()));
//
//        Set<UserRol> userRoles = new HashSet<>();
//
//        Rol rol = new Rol();
//        rol.setRolId(2L);
//        rol.setRolName("NORMAL");
//
//        UserRol usuarioRol = new UserRol();
//        usuarioRol.setUser(user);
//        usuarioRol.setRol(rol);
//
//        userRoles.add(usuarioRol);
//        return userService.saveUser(user,userRoles);
//    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/")
    public ResponseEntity<?> createUser(@Valid @RequestBody UserRequest userRequest) {
        try {
            User savedUser = userService.saveUser(userRequest);
            return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{userId}")
    public void deleteUser(@PathVariable Long userId) {
        userService.delete(userId);
    }


}




