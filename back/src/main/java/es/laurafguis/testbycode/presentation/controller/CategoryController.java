package es.laurafguis.testbycode.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.service.ICategoryService;

@RestController
@RequestMapping("/category")
@CrossOrigin("*")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;

    @PostMapping("/")
    public ResponseEntity<Category> saveCategory(@RequestBody Category category){
        Category categorySave = categoryService.create(category);
        return ResponseEntity.ok(categorySave);
    }

    @GetMapping("/{categoryId}")
    public Category getCategoryById(@PathVariable Long categoryId){
        return categoryService.find(categoryId);
    }

    @GetMapping("/")
    public ResponseEntity<?> getCategories(){
        return ResponseEntity.ok(categoryService.getAll());
    }

    @PutMapping("/")
    public Category updateCategories(@RequestBody Category category){
        return categoryService.update(category);
    }

    @DeleteMapping("/{categoryId}")
    public void deleteCategory(@PathVariable Long categoryId){
        categoryService.delete(categoryId);
    }
}




