package es.laurafguis.testbycode.presentation.request;

import lombok.Data;

import java.util.Objects;
import java.util.Set;

@Data
public class UserRequest {

    private String username;
    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private String phone;
    private String profile;
    private Set<RolRequest> roles;

    public UserRequest() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<RolRequest> getRoles() {
        return roles;
    }
    public void setRoles(Set<RolRequest> roles) {
        this.roles = roles;
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }



}
