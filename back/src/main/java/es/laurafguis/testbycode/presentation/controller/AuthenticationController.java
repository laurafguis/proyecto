package es.laurafguis.testbycode.presentation.controller;

import es.laurafguis.testbycode.common.exception.UserNotFoundException;
import es.laurafguis.testbycode.presentation.request.JwtRequest;
import es.laurafguis.testbycode.presentation.response.JwtResponse;
import es.laurafguis.testbycode.security.JwtUtils;
import es.laurafguis.testbycode.domain.entity.User;
import es.laurafguis.testbycode.domain.service.impl.UserDetailsServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@CrossOrigin("*")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/generate-token")
    public ResponseEntity<?> generarToken(@RequestBody JwtRequest jwtRequest) throws Exception {
        try{
            autenticar(jwtRequest.getUsername(),jwtRequest.getPassword());
        }catch (UserNotFoundException exception){
            exception.printStackTrace();
            throw new Exception("User not found");
        }
        UserDetails userDetails =  this.userDetailsService.
                loadUserByUsername(jwtRequest.getUsername());
        String token = this.jwtUtils.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void autenticar(String username,String password) throws Exception {
        try{
            authenticationManager.
                    authenticate(new UsernamePasswordAuthenticationToken(username,password));
        }catch (DisabledException exception){
            throw  new Exception("USER DISABLED " + exception.getMessage());
        }catch (BadCredentialsException e){
            throw  new Exception("Invalid credentials" + e.getMessage());
        }
    }

    @GetMapping("/actual-user")
    public User getUserActual(Principal principal){
        return (User) this.userDetailsService.loadUserByUsername(principal.getName());
    }
}
