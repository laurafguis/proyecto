package es.laurafguis.testbycode.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import es.laurafguis.testbycode.domain.entity.Question;
import es.laurafguis.testbycode.domain.entity.Quiz;
import es.laurafguis.testbycode.domain.service.IQuestionService;
import es.laurafguis.testbycode.domain.service.IQuizService;


import javax.validation.Valid;
import java.util.*;

@RestController
@RequestMapping("/question")
@CrossOrigin("*")
public class QuestionController {

    @Autowired
    private IQuestionService questionService;

    @Autowired
    private IQuizService quizService;

    public QuestionController(IQuestionService questionService, IQuizService quizService) {
        this.questionService = questionService;
        this.quizService = quizService;
    }


    @PostMapping("/")
    public ResponseEntity<Question> saveQuestion(@Valid @RequestBody Question question){
        return ResponseEntity.ok(questionService.create(question));
    }

    @PutMapping("/")
    public ResponseEntity<Question> updateQuestion(@Valid @RequestBody Question question){
        return ResponseEntity.ok(questionService.update(question));
    }

    @GetMapping("/quiz/{quizId}")
    public ResponseEntity<?> getQuestionsByQuiz(@PathVariable Long quizId){
        Quiz quiz = quizService.find(quizId);
        Set<Question> questions = quiz.getQuestions();

        List quizs = new ArrayList(questions);
        if(quizs.size() > Integer.parseInt(quiz.getNumeroDePreguntas())){
            quizs = quizs.subList(0,Integer.parseInt(quiz.getNumeroDePreguntas() + 1));
        }

        Collections.shuffle(quizs);
        return ResponseEntity.ok(quizs);
    }

    @GetMapping("/{questionId}")
    public Question getQuestionById(@PathVariable Long questionId){
        return questionService.find(questionId);
    }

    @DeleteMapping("/{questionId}")
    public void deleteQuestion(@PathVariable Long questionId){
        questionService.delete(questionId);
    }

    @GetMapping("/quiz/all/{quizId}")
    public ResponseEntity<?> listQuizQuestionsAsAdministrator
            (@PathVariable Long quizId){
        Quiz quiz = new Quiz();
        quiz.setQuizId(quizId);
        Set<Question> questions = questionService.getQuestionsFromQuiz(quiz);
        return ResponseEntity.ok(questions);
    }

    @PostMapping("/evaluate-quiz")
    public ResponseEntity<?> evaluateQuiz(@RequestBody List<Question> questions) {
        double puntosMaximos = 0;
        Integer respuestasCorrectas = 0;
        Integer intentos = 0;

        for (Question p : questions) {
            Question question = this.questionService.find(p.getQuestionId());
            if (question.getRespuesta().equals(p.getRespuestaDada())) {
                respuestasCorrectas++;
                double puntos = Double.parseDouble(questions.get(0).getQuiz().getPuntosMaximos()) / questions.size();
                puntosMaximos += puntos;
            }
            if (p.getRespuestaDada() != null) {
                intentos++;
            }
        }
        Map<String,Object> respuestas = new HashMap<>();
        respuestas.put("puntosMaximos",puntosMaximos);
        respuestas.put("respuestasCorrectas",respuestasCorrectas);
        respuestas.put("intentos",intentos);
        return ResponseEntity.ok(respuestas);
    }
}
