package es.laurafguis.testbycode.presentation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.entity.Quiz;
import es.laurafguis.testbycode.domain.service.IQuizService;

import java.util.List;


@RestController
@RequestMapping("/quiz")
@CrossOrigin("*")
public class QuizController {

    @Autowired
    private IQuizService quizService;

    @PostMapping("/")
    public ResponseEntity<Quiz> saveQuiz(@RequestBody Quiz quiz) {
        return ResponseEntity.ok(quizService.create(quiz));
    }

    @PutMapping("/")
    public ResponseEntity<Quiz> updateQuiz(@RequestBody Quiz quiz) {
        return ResponseEntity.ok(quizService.update(quiz));
    }

    @GetMapping("/")
    public ResponseEntity<?> listQuizs() {
        return ResponseEntity.ok(quizService.getAll());
    }

    @GetMapping("/{quizId}")
    public Quiz getQuiz(@PathVariable Long quizId) {
        return quizService.find(quizId);
    }

    @DeleteMapping("/{quizId}")
    public void deleteQuiz(@PathVariable Long quizId) {
        quizService.delete(quizId);
    }

    @GetMapping("/category/{categoryId}")
    public List<Quiz> listQuizzesByCategory(@PathVariable Long categoryId) {
        Category category = new Category();
        category.setCategoryId(categoryId);
        return quizService.listQuizsByCategory(category);
    }

    @GetMapping("/activo")
    public List<Quiz> listActivoQuizs() {
        return quizService.getActivoQuizs();
    }

    @GetMapping("/category/activo/{categoryId}")
    public List<Quiz> listActivoQuizzesByCategory(@PathVariable Long categoryId) {
        Category category = new Category();
        category.setCategoryId(categoryId);
        return quizService.getActivoQuizsByCategory(category);
    }
}
