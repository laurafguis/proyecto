package es.laurafguis.testbycode.persistence;


import org.springframework.data.jpa.repository.JpaRepository;

import es.laurafguis.testbycode.domain.entity.Question;
import es.laurafguis.testbycode.domain.entity.Quiz;

import java.util.Set;

public interface QuestionRepository extends JpaRepository<Question,Long> {

    Set<Question> findByQuiz(Quiz quiz);


}
