package es.laurafguis.testbycode.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import es.laurafguis.testbycode.domain.entity.Category;

public interface CategoryRepository extends JpaRepository<Category,Long> {

}
