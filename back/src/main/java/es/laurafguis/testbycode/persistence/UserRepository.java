package es.laurafguis.testbycode.persistence;


import es.laurafguis.testbycode.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
