package es.laurafguis.testbycode.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import es.laurafguis.testbycode.domain.entity.Category;
import es.laurafguis.testbycode.domain.entity.Quiz;

import java.util.List;

public interface QuizRepository extends JpaRepository<Quiz,Long> {

    List<Quiz> findByCategory(Category category);

    List<Quiz> findByActivo(Boolean status);

    List<Quiz> findByCategoryAndActivo(Category category, Boolean status);

}
