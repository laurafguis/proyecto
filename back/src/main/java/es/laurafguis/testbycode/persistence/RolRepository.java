package es.laurafguis.testbycode.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import es.laurafguis.testbycode.domain.entity.Rol;

public interface RolRepository extends JpaRepository<Rol,Long> {

    Rol findByRolName(String rolName);
}
