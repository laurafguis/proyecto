package es.laurafguis.testbycode.common.exception;

public class UserFoundException extends Exception {

    public UserFoundException() {
        super("The user with that username already exists, please enter a new one!");
    }

    public UserFoundException(String message) {
        super(message);
    }
}
