package es.laurafguis.testbycode.common.dto;

import java.util.List;

public class RolDTO {
    private Long rolId;
    private String rolName;
    private List<String> userRoles;
}

