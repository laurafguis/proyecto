package es.laurafguis.testbycode.common.dto;


import java.util.Set;

public class QuizDTO {
    private Long quizId;
    private String title;
    private String description;
    private String puntosMaximos;
    private String numeroDePreguntas;
    private boolean activo;
    private CategoryDTO category;
    private Set<QuestionDTO> questions;
}