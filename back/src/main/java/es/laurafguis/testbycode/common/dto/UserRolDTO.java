package es.laurafguis.testbycode.common.dto;

import es.laurafguis.testbycode.domain.entity.Rol;
import es.laurafguis.testbycode.domain.entity.User;

public class UserRolDTO {
    
    private String userRolId;
    private User user;
    private Rol rol;
   
}
