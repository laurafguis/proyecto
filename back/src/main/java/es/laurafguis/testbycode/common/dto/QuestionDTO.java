package es.laurafguis.testbycode.common.dto;

public class QuestionDTO {
    private Long id;
    private String title;
    private String content;
    private CategoryDTO category;
}
