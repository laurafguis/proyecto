package es.laurafguis.testbycode.common.exception;

public class UserNotFoundException extends Exception {

    public UserNotFoundException() {
        super("The user with that username does not exist in the database, please re-enter it!");
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}

