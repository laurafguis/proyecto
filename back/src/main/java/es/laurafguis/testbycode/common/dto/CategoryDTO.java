package es.laurafguis.testbycode.common.dto;

public class CategoryDTO {
    private Long id;
    private String name;
    private String description;
}