import { test, expect } from '@playwright/test';

const registrationUrl = 'http://localhost:4200/signup'; // Asegúrate de que la URL es correcta

test.describe('Pruebas del sistema de registro', () => {
  test('registro exitoso', async ({ page }) => {
    await page.goto(registrationUrl);

    // Completa los campos del formulario
    await page.fill('input[name="username"]', 'nuevo_usuario');
    await page.fill('input[name="password"]', 'contraseña_segura');
    await page.fill('input[name="email"]', 'email@example.com');
    await page.fill('input[name="firstName"]', 'Nombre');
    await page.fill('input[name="lastName"]', 'Apellido');
    await page.fill('input[name="phone"]', '123456789');

    await page.click('mat-form-field[ng-reflect-name="roles"]');
    await page.waitForSelector('.mat-option', { state: 'visible' });
    // Esperar y hacer clic en el checkbox para la opción 'ADMIN'
    await page.click('.mat-option:has-text("Admin") >> .mat-checkbox-inner-container');

    // Esperar y hacer clic en el checkbox para la opción 'USER'
    await page.click('.mat-option:has-text("User") >> .mat-checkbox-inner-container');


    // Envía el formulario
    await page.click('button:has-text("Registrar")');

    // Verifica la respuesta de éxito
    await expect(page.locator('.swal2-popup')).toHaveText('Usuario registrado con éxito en el sistema');
  });

  test('registro fallido por campos incompletos', async ({ page }) => {
    await page.goto(registrationUrl);
    await page.click('button:has-text("Registrar")');
    await expect(page.locator('.mat-snack-bar-container')).toHaveText('Debes de rellenar el formulario!!');
  });
});

