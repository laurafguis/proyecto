import { test, expect } from '@playwright/test';

// URL de la página de login
const loginUrl = 'http://localhost:4200/login';

test.describe('Pruebas de sistema de login', () => {

  test('login exitoso', async ({ page }) => {
    await page.goto(loginUrl);
    // Rellena los campos de usuario y contraseña con credenciales válidas
    await page.fill('input[name="username"]', 'laura.fernandez');
    await page.fill('input[name="password"]', '12345');
    await page.click('button.btn-login');
    await expect(page).toHaveURL(/.*user-dashboard\/0|admin/);
  });

  test('login fallido', async ({ page }) => {
    await page.goto(loginUrl);
    // Rellena los campos de usuario y contraseña con credenciales inválidas
    await page.fill('input[name="username"]', 'usuario_incorrecto');
    await page.fill('input[name="password"]', 'password_incorrecta');
    await page.click('button.btn-login');
    const snackBar = await page.locator('.mat-snack-bar-container');
    await expect(snackBar).toContainText('Datos incorrectos, vuelva a introducirlos');
  });

});


