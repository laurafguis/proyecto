import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-quiz',
  templateUrl: './add-quiz.component.html',
  styleUrls: ['./add-quiz.component.css']
})
export class AddQuizComponent {

  categories:any = [];

  quizData = {
    title:'',
    description:'',
    puntosMaximos:'',
    numeroDePreguntas:'',
    activo:true,
    category:{
      categoryId:''
    }
  }

  constructor(
    private categoryService:CategoryService,
    private snack:MatSnackBar,
    private quizService:QuizService,
    private router:Router) { }

  ngOnInit(): void {
    this.categoryService.getCategories().subscribe(
      (dato:any) => {
        this.categories = dato;
        console.log(this.categories);
      },(error) => {
        console.log(error);
        Swal.fire('Error !!','Error al cargar los datos','error');
      }
    )
  }

  guardarCuestionario(){
    console.log(this.quizData);
    if(this.quizData.title.trim() == '' || this.quizData.title == null){
      this.snack.open('El título es requerido','',{
        duration:3000
      });
      return ;
    }

    this.quizService.addQuiz(this.quizData).subscribe(
      (data) => {
        console.log(data);
        Swal.fire('Cuestionario guardado','El cuestioanrio ha sido guardado con éxito','success');
        this.quizData = {
          title : '',
          description : '',
          puntosMaximos : '',
          numeroDePreguntas : '',
          activo:true,
          category:{
            categoryId:''
          }
        }
        this.router.navigate(['/admin/quizs']);
      },
      (error) => {
        Swal.fire('Error','Error al guardar el cuestionario','error');
      }
    )
  }

}
