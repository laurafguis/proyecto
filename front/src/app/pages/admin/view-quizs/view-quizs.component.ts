import { Component, OnInit } from '@angular/core';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-view-quizs',
  templateUrl: './view-quizs.component.html',
  styleUrls: ['./view-quizs.component.css']
})
export class ViewQuizsComponent implements OnInit {

  quizs : any = [

  ]

  constructor(private quizService:QuizService) { }

  ngOnInit(): void {
    this.quizService.listQuizs().subscribe(
      (dato:any) => {
        this.quizs = dato;
        console.log(this.quizs);
      },
      (error) => {
        console.log(error);
        Swal.fire('Error','Error al cargar los exámenes','error');
      }
    )
  }

  deleteQuiz(quizId:any){
    Swal.fire({
      title:'Eliminar cuestionario',
      text:'¿Estás seguro de eliminar el cuestionario?',
      icon:'warning',
      showCancelButton:true,
      confirmButtonColor:'#0065ff',
      cancelButtonColor:'#ff3838',
      confirmButtonText:'Eliminar',
      cancelButtonText:'Cancelar'
    }).then((result) => {
      if(result.isConfirmed){
        this.quizService.deleteQuiz(quizId).subscribe(
          (data) => {
            this.quizs = this.quizs.filter((quiz:any) => quiz.quizId != quizId);
            Swal.fire('Cuestionario eliminado','El cuestionario ha sido eliminado de la base de datos','success');
          },
          (error) => {
            Swal.fire('Error','Error al eliminar el cuestionario','error');
          }
        )
      }
    })
  }
}

