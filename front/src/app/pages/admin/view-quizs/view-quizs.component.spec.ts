import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewQuizsComponent } from './view-quizs.component';

describe('ViewQuizsComponent', () => {
  let component: ViewQuizsComponent;
  let fixture: ComponentFixture<ViewQuizsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewQuizsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ViewQuizsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
