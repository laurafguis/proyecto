import { Component, OnInit, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import { Chart, ChartConfiguration, ChartData, registerables } from 'chart.js';

Chart.register(...registerables);

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit, AfterViewInit {
  @ViewChild('barChart') barChart!: ElementRef<HTMLCanvasElement>;
  @ViewChild('doughnutChart1') doughnutChart1!: ElementRef<HTMLCanvasElement>;
  @ViewChild('doughnutChart2') doughnutChart2!: ElementRef<HTMLCanvasElement>;

  barChartInstance!: Chart;
  doughnutChart1Instance!: Chart;
  doughnutChart2Instance!: Chart;

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.initializeCharts();
  }

  initializeCharts() {
    this.initializeBarChart();
    this.initializeDoughnutChart1();
    this.initializeDoughnutChart2();
  }

  initializeBarChart() {
    if (this.barChart && this.barChart.nativeElement) {
      const chartData: ChartData<'bar'> = {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo'],
        datasets: [
          {
            label: 'Puntuación Media',
            data: [65, 59, 80, 81, 56],
            backgroundColor: 'rgba(183, 241, 174, 0.2)',
            borderColor: '#366a34',
            borderWidth: 1
          }
        ]
      };

      const chartConfig: ChartConfiguration<'bar'> = {
        type: 'bar',
        data: chartData,
        options: {
          responsive: true,
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      };

      this.barChartInstance = new Chart(this.barChart.nativeElement, chartConfig);
    } else {
      console.error('No se pudo encontrar el elemento canvas para el gráfico.');
    }
  }

  initializeDoughnutChart1() {
    if (this.doughnutChart1 && this.doughnutChart1.nativeElement) {
      const chartData: ChartData<'doughnut'> = {
        labels: ['Correctas', 'Incorrectas', 'No Respondidas'],
        datasets: [
          {
            data: [60, 30, 10],
            backgroundColor: ['#36A2EB', '#FF6384', '#FFCE56'],
            hoverBackgroundColor: ['#36A2EB', '#FF6384', '#FFCE56']
          }
        ]
      };

      const chartConfig: ChartConfiguration<'doughnut'> = {
        type: 'doughnut',
        data: chartData,
        options: {
          responsive: true
        }
      };

      new Chart(this.doughnutChart1.nativeElement, chartConfig);
    } else {
      console.error('No se pudo encontrar el elemento canvas para el gráfico de dona 1.');
    }
  }

  initializeDoughnutChart2() {
    if (this.doughnutChart2 && this.doughnutChart2.nativeElement) {
      const chartData: ChartData<'doughnut'> = {
        labels: ['Aprobados', 'Reprobados'],
        datasets: [
          {
            data: [75, 25],
            backgroundColor: ['#4BC0C0', '#FF9F40'],
            hoverBackgroundColor: ['#4BC0C0', '#FF9F40']
          }
        ]
      };

      const chartConfig: ChartConfiguration<'doughnut'> = {
        type: 'doughnut',
        data: chartData,
        options: {
          responsive: true
        }
      };

      new Chart(this.doughnutChart2.nativeElement, chartConfig);
    } else {
      console.error('No se pudo encontrar el elemento canvas para el gráfico de dona 2.');
    }
  }
}
