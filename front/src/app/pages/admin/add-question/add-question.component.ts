import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from 'src/app/services/question.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent {


  quizId:any;
  title:any;
  question:any = {
    quiz : {},
    content : '',
    opcion1 : '',
    opcion2 : '',
    opcion3 : '',
    opcion4 : '',
    respuesta : ''
  }

  constructor(
    private route:ActivatedRoute,
    private questionService:QuestionService) { }

  ngOnInit(): void {
    this.quizId = this.route.snapshot.params['quizId'];
    this.title = this.route.snapshot.params['title'];
    this.question.quiz['quizId'] = this.quizId;
  }

  formSubmit(){
    if(this.question.content.trim() == '' || this.question.content == null){
      return;
    }
    if(this.question.opcion1.trim() == '' || this.question.opcion1 == null){
      return;
    }
    if(this.question.opcion2.trim() == '' || this.question.opcion2 == null){
      return;
    }
    if(this.question.opcion3.trim() == '' || this.question.opcion3 == null){
      return;
    }
    if(this.question.opcion4.trim() == '' || this.question.opcion4 == null){
      return;
    }
    if(this.question.respuesta.trim() == '' || this.question.respuesta == null){
      return;
    }

    this.questionService.saveQuestion(this.question).subscribe(
      (data) => {
        Swal.fire('Pregunta guardada','La pregunta ha sido agregada con éxito','success');
        this.question.content = '';
        this.question.opcion1 = '';
        this.question.opcion2 = '';
        this.question.opcion3 = '';
        this.question.opcion4 = '';
        this.question.respuesta = '';
      },(error) => {
        Swal.fire('Error','Error al guardar la pregunta en la base de datos','error');
        console.log(error);
      }
    )
  }

}

