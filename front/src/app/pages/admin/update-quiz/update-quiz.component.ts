import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CategoryService } from 'src/app/services/category.service';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-update-quiz',
  templateUrl: './update-quiz.component.html',
  styleUrls: ['./update-quiz.component.css']
})
export class UpdateQuizComponent {

  constructor(
    private route: ActivatedRoute,
    private quizService: QuizService,
    private categoryService: CategoryService,
    private router: Router) { }

  quizId = 0;
  quiz: any;
  categories: any;

  ngOnInit(): void {
    this.quizId = this.route.snapshot.params['quizId'];
    this.quizService.getQuiz(this.quizId).subscribe(
      (data) => {
        this.quiz = data;
        console.log(this.quiz);
      },
      (error) => {
        console.log(error);
      }
    )

    this.categoryService.getCategories().subscribe(
      (data: any) => {
        this.categories = data;
      },
      (error) => {
        alert('Error al cargar las categorías');
      }
    )
  }

  public actualizarDatos() {
    this.quizService.addQuiz(this.quiz).subscribe(
      (data) => {
        Swal.fire('Cuestionario actualizado', 'El cuestionario ha sido actualizado con éxito', 'success').then(
          (e) => {
            this.router.navigate(['/admin/quizs']);
          }
        );
      },
      (error) => {
        Swal.fire('Error en el sistema', 'No se ha podido actualizar el cuestionario', 'error');
        console.log(error);
      }
    )
  }
}

