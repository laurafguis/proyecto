import { chromium, Browser, Page } from 'playwright';

let browser: Browser;
let page: Page;

beforeAll(async () => {
  browser = await chromium.launch();
});
afterAll(async () => {
  await browser.close();
});
beforeEach(async () => {
  page = await browser.newPage();
});
afterEach(async () => {
  await page.close();
});

it('debería iniciar sesión con credenciales válidas', async () => {
  await page.goto('http://localhost:4200/login');

  await page.fill('input[name="username"]', 'usuario_valido');
  await page.fill('input[name="password"]', 'contraseña_valida');

  await page.click('button[name="login"]');

  await page.waitForNavigation();

  const url = page.url();
  expect(url).toMatch(/\/admin|\/user-dashboard\/0/);
});
