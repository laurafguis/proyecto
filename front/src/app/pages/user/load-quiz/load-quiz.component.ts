import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuizService } from 'src/app/services/quiz.service';

@Component({
  selector: 'app-load-quiz',
  templateUrl: './load-quiz.component.html',
  styleUrls: ['./load-quiz.component.css']
})
export class LoadQuizComponent {

  catId:any;
  quizs:any;

  constructor(
    private route:ActivatedRoute,
    private quizService: QuizService
  ) { }

  ngOnInit(): void {
      this.route.params.subscribe((params) => {
        this.catId = params['catId'];

        if(this.catId == 0){
          console.log("Cargando todos los cuestionarios");
          this.quizService.getActivoQuizs().subscribe(
            (data) => {
              this.quizs = data;
              console.log(this.quizs);
            },
            (error) => {
              console.log(error);
            }
          )
        }
        else{
          console.log("Cargando un cuestionario");
          this.quizService.getActivoQuizsByCategory(this.catId).subscribe(
            (data:any) => {
              this.quizs = data;
              console.log(this.quizs);
            },
            (error) => {
              console.log(error);
            }
          )
        }
      })
  }

}
