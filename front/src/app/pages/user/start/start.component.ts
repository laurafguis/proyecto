import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LocationStrategy } from '@angular/common';
import Swal from 'sweetalert2';
import { QuestionService } from 'src/app/services/question.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  quizId: any;
  questions: any;
  puntosConseguidos = 0;
  respuestasCorrectas = 0;
  intentos = 0;
  isSubmitted = false;
  timer: any;
  interval: any;

  constructor(
    private locationSt: LocationStrategy,
    private route: ActivatedRoute,
    private questionService: QuestionService
  ) { }

  ngOnInit(): void {
    this.preventBackButton();
    this.quizId = this.route.snapshot.params['quizId'];
    console.log(this.quizId);
    this.loadQuestions();
  }

  loadQuestions() {
    this.questionService.listQuizQuestionsForTest(this.quizId).subscribe(
      (data: any) => {
        console.log(data);
        this.questions = data;

        this.timer = this.questions.length * 2 * 60; // asumiendo 2 minutos por pregunta

        this.questions.forEach((question: any) => {
          question['respuestaDada'] = '';
        });
        console.log(this.questions);
        this.startTimer();
      },
      (error) => {
        console.log(error);
        Swal.fire('Error', 'Failed to load quiz questions', 'error');
      }
    )
  }

  startTimer() {
    this.interval = setInterval(() => {
      if (this.timer <= 0) {
        this.evaluateQuiz();
        clearInterval(this.interval);
      } else {
        this.timer--;
      }
    }, 1000);
  }

  preventBackButton() {
    history.pushState(null, null!, location.href);
    this.locationSt.onPopState(() => {
      history.pushState(null, null!, location.href);
    });
  }

  onAnswerSelected(question: any, index: number) {
    const selectedAnswer = this.questions[index].respuestaDada;
    if (selectedAnswer) {
      this.questions[index].respuestaDada = selectedAnswer;
    } else {
      Swal.fire('Error', 'Debes seleccionar una respuesta', 'error');
    }
  }

  submitQuiz() {
    Swal.fire({
      title: '¿Deseas enviar el cuestionario?',
      showCancelButton: true,
      cancelButtonText: 'Cancelar',
      confirmButtonText: 'Enviar',
      icon: 'info'
    }).then((result) => {
      if (result.isConfirmed) {
        this.evaluateQuiz();
      }
    });
  }

  evaluateQuiz() {
    clearInterval(this.interval); // Detener el temporizador al evaluar
    this.questionService.evaluateQuiz(this.questions).subscribe(
      (data: any) => {
        console.log(data);
        this.puntosConseguidos = data.puntosMaximos;
        this.respuestasCorrectas = data.respuestasCorrectas;
        this.intentos = data.attempts;
        this.isSubmitted = true;
      },
      (error) => {
        console.log(error);
      }
    );
  }

  getFormattedTime() {
    let mm = Math.floor(this.timer / 60);
    let ss = this.timer % 60;
    return `${mm}:${ss < 10 ? '0' : ''}${ss} seg.`;
  }

  printPage() {
    window.print();
  }
}
