import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { QuizService } from 'src/app/services/quiz.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-instructions',
  templateUrl: './instructions.component.html',
  styleUrls: ['./instructions.component.css']
})
export class InstructionsComponent implements OnInit {

  quizId:any;
  quiz:any = new Object();

  constructor(
    private quizService:QuizService,
    private route:ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    this.quizId = this.route.snapshot.params['quizId'];
    this.quizService.getQuiz(this.quizId).subscribe(
      (data:any) => {
        console.log(data);
        this.quiz = data;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  startQuiz(){
    Swal.fire({
      title:'¿Quieres comenzar el Cuestionario?',
      showCancelButton:true,
      cancelButtonText:'Cancelar',
      confirmButtonText:'Empezar',
      icon:'info'
    }).then((result:any) => {
      if(result.isConfirmed){
        this.router.navigate(['/start/'+this.quizId]);
      }
    })
  }

}
