import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { APP_ROLES } from 'src/app/constans/roles';
import { User } from 'src/app/interfaces/user';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  public user: User = {
    username: '',
    password: '',
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    profile: '',
    roles: [],
  }

  roles = Object.keys(APP_ROLES).map(key => APP_ROLES[key as keyof typeof APP_ROLES]);
  selectedRoles: string[] = [];


  constructor(private userService: UserService, private snack: MatSnackBar) { }

  ngOnInit(): void {
  }

  formSubmit() {
    console.log(this.user);
    if (this.user.username == '' || this.user.username == null) {
      this.snack.open('Debes de rellenar el formulario!!', 'Aceptar', {
        duration: 3000,
        verticalPosition: 'bottom',
        horizontalPosition: 'right'
      });
      return;
    }

    this.user.roles = this.selectedRoles.map(rol => ({ rolName: rol }));

    this.userService.addUser(this.user).subscribe(
      (data) => {
        console.log(data);
        Swal.fire('Usuario guardado', 'Usuario registrado con éxito en el sistema', 'success');
      }, (error) => {
        console.log(error);
        this.snack.open('Ha ocurrido un error en el sistema !!', 'Aceptar', {
          duration: 3000
        });
      }
    )
  }

  clearForm() {
    this.user = {
      username: '',
      password: '',
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      profile: '',
      roles: []
    };
    this.selectedRoles = [];
  }

  addRole(role: string) {
    if (!this.selectedRoles.includes(role)) {
      this.selectedRoles.push(role);
    }
  }

  removeRole(role: string) {
    this.selectedRoles = this.selectedRoles.filter(r => r !== role);
  }

}
