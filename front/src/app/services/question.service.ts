import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import baserUrl from '../utils/helper';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor(private http: HttpClient) { }

  public listQuizQuestions(quizId: any) {
    return this.http.get(`${baserUrl}/question/quiz/all/${quizId}`);
  }

  public saveQuestion(question: any) {
    return this.http.post(`${baserUrl}/question/`, question);
  }

  public deleteQuestion(questionId: any) {
    return this.http.delete(`${baserUrl}/question/${questionId}`);
  }

  public updateQuestion(question: any) {
    return this.http.put(`${baserUrl}/question/`, question);
  }

  public getQuestion(questionId: any) {
    return this.http.get(`${baserUrl}/question/${questionId}`);
  }

  public listQuizQuestionsForTest(quizId: any) {
    return this.http.get(`${baserUrl}/question/quiz/all/${quizId}`);
  }

  public evaluateQuiz(questions: any) {
    return this.http.post(`${baserUrl}/question/evaluate-quiz`, questions);
  }
}
