import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { Observable, of, throwError } from 'rxjs';
import baserUrl from '../utils/helper';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  public getUsers(): Observable<User[]> {
    return this.httpClient.get<User[]>(`${baserUrl}/users/`);
  }

  public getUser(id: number): Observable<User> {
    return this.httpClient.get<User>(`${baserUrl}/users/${id}`);
  }

  public updateUser(user: User): Observable<any> {
    return this.httpClient.put(`${baserUrl}/users/${user.id}`, user);
  }

  public addUser(user:any){
    return this.httpClient.post(`${baserUrl}/users/`,user);
  }

  private handleError(error: HttpErrorResponse) {
    return throwError(() => new Error('Algo salió mal; por favor, intenta de nuevo más tarde.'));
  }

}
