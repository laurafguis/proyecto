import { Injectable } from '@angular/core';
import baserUrl from '../utils/helper';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

  public loginStatusSubject = new Subject<boolean>();
  
  constructor(private http: HttpClient) { }

  public generateToken(loginData: any) {
    return this.http.post(`${baserUrl}/generate-token`, loginData);
  }
  public getCurrentUser() {
    return this.http.get(`${baserUrl}/actual-user`);
  }
  public loginUser(token: any) {
    localStorage.setItem('token', token);
    return true;
  }
  public isLoggedIn() {
    const tokenStr = localStorage.getItem('token');
    return !!tokenStr;
  }

  public logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    return true;
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  public setUser(user: any) {
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUser() {
    const userStr = localStorage.getItem('user');
    if (userStr) {
      return JSON.parse(userStr);
    } else {
      this.logout();
      return null;
    }
  }

  public getUserRole() {
    const user = this.getUser();
    return user?.authorities[0]?.authority;
  }
}
