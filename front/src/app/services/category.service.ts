import { Injectable } from '@angular/core';
import baserUrl from '../utils/helper';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {


  constructor(private http:HttpClient) { }

  public getCategories(){
    return this.http.get(`${baserUrl}/category/`);
  }

  public addCategory(category:any){
    return this.http.post(`${baserUrl}/category/`,category);
  }
}
