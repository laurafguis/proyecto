import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import baserUrl from '../utils/helper';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(private http: HttpClient) { }

  public listQuizs() {
    return this.http.get(`${baserUrl}/quiz/`);
  }

  public addQuiz(quiz: any) {
    return this.http.post(`${baserUrl}/quiz/`, quiz);
  }

  public deleteQuiz(quizId: any) {
    return this.http.delete(`${baserUrl}/quiz/${quizId}`);
  }

  public getQuiz(quizId: any) {
    return this.http.get(`${baserUrl}/quiz/${quizId}`);
  }

  public updateQuiz(quiz: any) {
    return this.http.put(`${baserUrl}/quiz/`, quiz);
  }

  public listQuizsByCategory(categoryId: any) {
    return this.http.get(`${baserUrl}/quiz/category/${categoryId}`);
  }

  public getActivoQuizs() {
    return this.http.get(`${baserUrl}/quiz/activo`);
  }

  public getActivoQuizsByCategory(categoryId: any) {
    return this.http.get(`${baserUrl}/quiz/category/activo/${categoryId}`);
  }
}
