import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

 isLoggedIn = false;
  user: any = null;

  constructor(public login: LoginService) { }

  ngOnInit(): void {
    this.updateLoginStatus();
    this.login.loginStatusSubject.asObservable().subscribe(() => {
      this.updateLoginStatus();
    });
  }

  private updateLoginStatus(): void {
    this.isLoggedIn = this.login.isLoggedIn();
    this.user = this.login.getUser();
  }

  
  public logout() {
    this.login.logout();
    window.location.reload();
  }
}



