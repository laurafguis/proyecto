export const APP_ROLES = {
  ADMIN: { rolName: 'Admin' },
  NORMAL: { rolName: 'User' }
} as const;
