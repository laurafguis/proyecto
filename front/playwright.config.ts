import { PlaywrightTestConfig } from "@playwright/test";

const config: PlaywrightTestConfig = {
  testDir: './e2e', // Directorio de pruebas
  timeout: 30000,
  retries: 1,
  use: {
    baseURL: 'http://localhost:4200',
    headless: false,
    screenshot: 'on',
    video: 'on',
  },
  projects: [
    {
      name: 'Chrome',
      use: {
        browserName: 'chromium',
        channel: 'chrome',
      },
    },
  ],
  reporter: 'html',
};

export default config;
